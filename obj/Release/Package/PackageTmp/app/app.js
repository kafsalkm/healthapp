
var app = angular.module('app', ['ui.router', 'ui.bootstrap', 'flash','LocalStorageModule','angular-linq','dx','chart.js',
    //main modules
     'dashboard']);


app.config(['$stateProvider', '$locationProvider', '$urlRouterProvider', function ($stateProvider, $locationProvider, $urlRouterProvider, $modalInstance) {

    //routing
    $stateProvider
       .state('app', {
           url: '/app',
           templateUrl: 'app/common/app.html',
           controller: 'appCtrl',
           controllerAs: 'vm',
           data: {
               pageTitle: ''
           }
       });

    $urlRouterProvider.otherwise('app/home');
}]);

// set global configuration of application and it can be accessed by injecting appSettings in any modules
app.constant('appSettings', appConfig);
