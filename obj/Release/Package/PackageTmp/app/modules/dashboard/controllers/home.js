﻿
dashboard.controller("HomeController", ['$rootScope', '$scope', '$state',
function ($rootScope, $scope, $state) {
    var vm = this;

    $scope.userName = "Jana";
    $scope.status = "allrigth";
    $scope.temperature = 36.75;
    $scope.kcal = 537;
    $scope.steps = 3422;
    $scope.bpm = 87;
    var stepsTarget = 10000;
    var stepsCount = 3422;
    var stepsperc = stepsCount / stepsTarget * 100;
    $scope.width = { "width": stepsperc + "%" };

    $scope.labels = ["08", "12", "16", "20", "00"];
    $scope.series = ['Today', 'Avg'];
    $scope.data = [
      [65, 59, 80, 81, 56],
      [28, 48, 40, 19, 86]
    ];
    $scope.datasetOverride = [{ yAxisID: 'y-axis-1' }, { yAxisID: 'y-axis-2' }];
    $scope.options = {
        scales: {
            yAxes: [
              {
                  id: 'y-axis-1',
                  type: 'linear',
                  display: true,
                  position: 'left'
              },
              {
                  id: 'y-axis-2',
                  type: 'linear',
                  display: true,
                  position: 'right'
              }
            ]
        }
    };


    var calburned = [
    { month: 1, 2010: 37 }, { month: 2, 2010: 28 },
    { month: 3, 2010: 34 }, { month: 4, 2010: 37 },
    { month: 5, 2010: 47 }, { month: 6, 2010: 30 }
    ];

    var heartrate = [{ hour: 0, rate: 80 },
    { hour: 1, rate: 80 }, { hour: 2, rate: 120 },
    { hour: 3, rate: 80 }, { hour: 4, rate: 120 },
    { hour: 5, rate: 80 }, { hour: 6, rate: 120 },
    { hour: 7, rate: 80 }, { hour: 8, rate: 120 },
    { hour: 9, rate: 80 }
    ];

    var temprature = [{ hour: 0, rate: 37.5 },
    { hour: 1, temp: 37.3 }, { hour: 2, temp: 37 },
    { hour: 3, temp: 37.2 }, { hour: 4, temp: 37.4 },
    { hour: 5, temp: 37 }, { hour: 6, temp: 37.4 },
    { hour: 7, temp: 37.6 }, { hour: 8, temp: 37.1 },
    { hour: 9, temp: 36.7 }
    ];

    $scope.sparklineInstance = {};
    $scope.sparklineInstance1 = {};
    $scope.sparklineInstance2 = {};

    $scope.sparklineConfiguration = {
        dataSource: calburned,
        argumentField: 'month',
        valueField: '2010',
        type: 'spline',
        lineColor: 'aqua',
        showMinMax: true,
        onInitialized: function (e) {
            $scope.sparklineInstance = e.component;
        }
    };

    $scope.sparklineHeartRate = {
        dataSource: heartrate,
        argumentField: 'hour',
        valueField: 'rate',
        type: 'spline',
        lineColor: 'palevioletred',
        showMinMax: true,
        onInitialized: function (e) {
            $scope.sparklineInstance1 = e.component;
        }
    };


    $scope.sparklineTemprature = {
        dataSource: temprature,
        argumentField: 'hour',
        valueField: 'temp',
        type: 'spline',
        lineColor: 'coral',
        showMinMax: true,
        onInitialized: function (e) {
            $scope.sparklineInstance2 = e.component;
        }
    };
}]);

