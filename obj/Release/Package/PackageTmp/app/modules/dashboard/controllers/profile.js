﻿/// <reference path="websites.js" />

dashboard.controller("StatusController", ['$rootScope', '$scope', '$state', '$location', 'dashboardService', 'Flash',
function ($rootScope, $scope, $state, $location, dashboardService, Flash) {
    var vm = this;
    
    $scope.userName = "Jana";
    $scope.status = "good";
    $scope.age = 22;
    $scope.bg = "A";
    $scope.bmi = 20.4;
    $scope.heigth = 172;
    $scope.weigth = 53;
    $scope.fat = 21;

    //weight loose progress bar
    $scope.weightlost = 4.2;
    $scope.weightlooseTarget = 8;
    $scope.weightloosePerc = $scope.weightlost / $scope.weightlooseTarget * 100;
    $scope.lwWidth = { "width": $scope.weightloosePerc + "%" };

    //running progress bar
    $scope.run = 8.2;
    $scope.runTarget = 10;
    $scope.runPerc = $scope.run / $scope.runTarget * 100;
    $scope.rpwWidth = { "width": $scope.runPerc + "%" };

    //steps progress bar
    $scope.stepsCount = 3422;
    $scope.stepsTarget = 10000;
    var stepsperc = $scope.stepsCount / $scope.stepsTarget * 100;
    $scope.spdWidth = { "width": stepsperc + "%" };


    //goal progress bar
    $scope.goalPerc = ($scope.weightloosePerc + $scope.runPerc + stepsperc) / 3;
    $scope.goalWidth = { "width": $scope.goalPerc + "%" };

    // change state on progressbar click
    $scope.onGoalClick = function () {

        $state.go('app.candaccount');
    }

}]);

