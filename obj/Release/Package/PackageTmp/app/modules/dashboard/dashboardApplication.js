﻿
var dashboard = angular.module('dashboard', ['ui.router', 'ngAnimate', 'ngMaterial', 'dx', 'chart.js']);


dashboard.config(["$stateProvider", function ($stateProvider) {

    // Portfolio page state
    $stateProvider.state('app.home', {
        url: '/home',
        templateUrl: 'app/modules/dashboard/views/home.html',
        controller: 'HomeController',
        controllerAs: 'vm',
        data: {
            pageTitle: 'Home'
        }
    });

    //Websites page state
    $stateProvider.state('app.profile', {
        url: '/profile',
        templateUrl: 'app/modules/dashboard/views/profile.html',
        controller: 'StatusController',
        controllerAs: 'vm',
        data: {
            pageTitle: 'Profile'
        }
    });

    //Gallery page state
    $stateProvider.state('app.goal', {
        url: '/goal',
        templateUrl: 'app/modules/dashboard/views/goal.html',
        controller: 'GoalController',
        controllerAs: 'vm',
        data: {
            pageTitle: 'MyGoals'
        }
    });
   
}]);

