﻿
dashboard.controller("GoalController", ['$rootScope', '$scope', '$state', '$location', 'dashboardService', 'Flash','$filter',
function ($rootScope, $scope, $state, $location, dashboardService, Flash, $filter) {
    var vm = this;


    $scope.labels = ["Feb", "Mar", "Apr", "May", "June", "July"];
    $scope.series = ['Body Weight'];
    $scope.data = [
      [57, 55, 55, 54, 56, 55, 53]
    ];
    $scope.datasetOverride = [{ yAxisID: 'y-axis-1' }, { yAxisID: 'y-axis-2' }];
    $scope.options = {
        scales: {
            yAxes: [
              {
                  id: 'y-axis-1',
                  type: 'linear',
                  display: false,
                  position: 'left'
              },
              {
                  id: 'y-axis-2',
                  type: 'linear',
                  display: false,
                  position: 'right'
              }
            ]
        }
    };
    var today = new Date();
    var minDate = new Date(2017, 01, 05, 0, 0, 0);
    var maxDate = new Date(2017, 05, 01, 0, 0, 0);
    $scope.minDate = $filter('date')(minDate, 'MMM dd yyyy');
    $scope.today = $filter('date')(today, 'MMM dd yyyy');
    $scope.maxDate = $filter('date')(maxDate, 'MMM dd yyyy');

    $scope.weight = 54.7;
    $scope.minweight = 53.4;
    $scope.maxweight = 57.3;
    $scope.goalweigth = 50;
}]);

