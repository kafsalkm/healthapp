﻿

var appConfig = {
    title: "Corporate Directory",
    lang: "en",
    dateFormat: "mm/dd/yy",
    theme: 'skin-blue',
    layout: "layout-boxed"
};